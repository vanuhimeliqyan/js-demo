var count = 1;

// first part is done
console.log("First part");
console.log("__________________________________");
while (count <= 10){
	if (count < 10){
		console.log("Count is: " + count);	
	} else {
		console.log("Count is: " + count + ": Finished");
	}	
	count = count + 1;
}

// second part
console.log();
console.log("Second part");
console.log("__________________________________");
var secondCount = 2;
while (secondCount <= 10){
	if (secondCount < 10){
		console.log("Count is: " + secondCount);	
	} else {
		console.log("Count is: " + secondCount + ": Finished");
	}	
	secondCount = secondCount + 2;
}

// third part
var thirdCount;
for (thirdCount = 1; thirdCount <= 10; thirdCount = thirdCount + 1){
	console.log("the count is: " + thirdCount);
}
console.log("FInished")

// forth part for loop 'for'
var forthCount;
for (forthCount = 1; forthCount <= 10; forthCount = forthCount + 2){
    if (forthCount < 9){
        console.log("the count is:" + forthCount);
    } else {
        console.log( "the count is: " + forthCount + " Finished");
        console.log("*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^");
    }
}
